package com.mp.learn.it.spi;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.CurrentUserProvider;

public class SettableAuthenticator implements CurrentUserProvider {

  @Nullable
  private UserId currentUserId;

  @Override
  public UserId getCurrentUserId() {
    checkCurrentUserSet();
    return currentUserId;
  }

  public void set(UserId userId) {
    this.currentUserId = Preconditions.checkNotNull(userId);
  }

  private void checkCurrentUserSet() {
    if (currentUserId == null) {
      throw new SettableAuthenticatorFault();
    }
  }
}
