package com.mp.learn.it.spi;

import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.UserDao;
import com.mp.learn.gs.business.spi.value.UserState;

public class InMemoryUserDao implements UserDao {

  private final InMemoryDaoSupport<UserId, UserState> support;

  public InMemoryUserDao() {
    this.support = new InMemoryDaoSupport<>(UserState::id);
  }
  
  @Override
  public UserState load(UserId userId) {
    return support.load(userId);
  }

  @Override
  public Iterable<UserState> loadBatch(Iterable<UserId> userIds) {
    return support.loadBatch(userIds);
  }

  @Override
  public void save(UserState state) {
    support.save(state);
  }

  @Override
  public void delete(UserId userId) {
    support.delete(userId);
  }
}
