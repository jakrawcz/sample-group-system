package com.mp.learn.it.spi;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class InMemoryDaoSupport<I, T> {

  private final Function<T, I> idExtractor;

  private final Map<I, T> states;

  public InMemoryDaoSupport(Function<T, I> idExtractor) {
    this.idExtractor = idExtractor;
    this.states = new HashMap<>();
  }

  public T load(I id) {
    synchronized (states) {
      @Nullable T state = states.get(id);
      checkThat(state != null);
      return state;
    }
  }

  public Iterable<T> loadBatch(Iterable<I> ids) {
    synchronized (states) {
      return Iterables.transform(ids, this::load);
    }
  }
  
  public Iterable<T> query(Predicate<T> predicate) {
    synchronized (states) {
      return Iterables.filter(states.values(), predicate);
    }
  }

  public void save(T state) {
    synchronized (states) {
      states.put(idExtractor.apply(state), state);
    }
  }

  public void saveBatch(Iterable<T> stateBatch) {
    synchronized (states) {
      stateBatch.forEach(this::save);
    }
  }

  public void delete(I id) {
    synchronized (states) {
      states.remove(id);
    }
  }
  
  private void checkThat(boolean condition) {
    if (!condition) {
      throw new InMemoryDaoFault();
    }
  }
}
