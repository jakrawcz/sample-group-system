package com.mp.learn.it.spi;

import java.util.Optional;

import com.google.common.base.Predicate;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.value.GroupCriteria;
import com.mp.learn.gs.business.spi.value.GroupState;

public class InMemoryGroupDao implements GroupDao {

  private final InMemoryDaoSupport<GroupId, GroupState> support;

  public InMemoryGroupDao() {
    this.support = new InMemoryDaoSupport<>(GroupState::id);
  }
  
  @Override
  public Iterable<GroupState> query(GroupCriteria groupCriteria) {
    return support.query(toPredicate(groupCriteria));
  }

  @Override
  public void save(GroupState state) {
    support.save(state);
  }

  @Override
  public void saveBatch(Iterable<GroupState> states) {
    support.saveBatch(states);
  }
  
  @Override
  public GroupState load(GroupId groupId) {
    return support.load(groupId);
  }

  private Predicate<GroupState> toPredicate(GroupCriteria groupCriteria) {
    return state -> {
      if (!containsMember(state, groupCriteria.requiredMemberId())) {
        return false;
      }
      return true;
    };
  }

  private boolean containsMember(GroupState state, Optional<UserId> userId) {
    return !userId.isPresent()
        || state.getPotentialMembershipOf(userId.get()).isPresent();
  }
}
