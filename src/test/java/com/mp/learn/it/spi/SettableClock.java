package com.mp.learn.it.spi;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import com.google.common.base.Preconditions;

public class SettableClock extends Clock {

  private Instant instant;

  public SettableClock() {
    this.instant = Instant.ofEpochMilli(0);
  }
  
  @Override
  public ZoneId getZone() {
    return Clock.systemUTC().getZone();
  }

  @Override
  public Clock withZone(ZoneId zone) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Instant instant() {
    return instant;
  }

  public void set(Instant instant) {
    this.instant = Preconditions.checkNotNull(instant);
  }
}
