package com.mp.learn.gs.business.spi.value;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.UserId;

import autovalue.shaded.com.google.common.common.base.Optional;
import autovalue.shaded.com.google.common.common.collect.Iterables;
import autovalue.shaded.com.google.common.common.collect.Lists;

/**
 * A state of a Group.
 */
public class GroupState {

  private final GroupId id;
  private final Instant creationInstant;

  private String name;
  private int limit;
  
  private final List<MembershipState> memberships;

  /**
   * Creates an instance.
   */
  public static GroupState create(GroupId id, Instant creationInstant, String name, int limit,
      Iterable<MembershipState> memberships) {
    return new GroupState(
        Preconditions.checkNotNull(id),
        Preconditions.checkNotNull(creationInstant), 
        Preconditions.checkNotNull(name),
        limit,
        Lists.newArrayList(memberships));
  }

  /**
   * Returns an instance which additionally contains the given membership.
   */
  public GroupState withMembership(MembershipState membership) {
    GroupState copy = new GroupState(this);
    copy.memberships.add(membership);
    return copy;
  }

  /**
   * Returns an instance from which the given membership is removed (if it has existed).
   */
  public GroupState withoutMembership(MembershipState membership) {
    GroupState copy = new GroupState(this);
    copy.memberships.remove(membership);
    return copy;
  }

  /**
   * Returns an instance with the given limit.
   */
  public GroupState withLimit(int limit) {
    GroupState copy = new GroupState(this);
    copy.limit = limit;
    return copy;
  }
  
  /**
   * An instant at which the Group has been created.
   */
  public Instant creationInstant() {
    return creationInstant;
  }

  /**
   * Memberships.
   */
  public List<MembershipState> memberships() {
    return Collections.unmodifiableList(memberships);
  }

  /**
   * An id.
   */
  public GroupId id() {
    return id;
  }
  
  /**
   * A name.
   */
  public String name() {
    return name;
  }

  /**
   * A size limit of the Group.
   */
  public int limit() {
    return limit;
  }
  
  /**
   * Returns a member of the given {@link UserId}, if it is contained in the Group.
   */
  public Optional<MembershipState> getPotentialMembershipOf(UserId userId) {
    return Iterables.tryFind(memberships(), membership -> membership.userId().equals(userId));
  }
  
  private GroupState(GroupId id, Instant creationInstant, String name, int limit,
      List<MembershipState> memberships) {
    this.id = id;
    this.creationInstant = creationInstant;
    this.name = name;
    this.limit = limit;
    this.memberships = memberships;
  }

  private GroupState(GroupState prototype) {
    this(prototype.id, prototype.creationInstant, prototype.name, prototype.limit, 
        new ArrayList<>(prototype.memberships));
  }
}
