package com.mp.learn.gs.business.impl.asm;

import com.mp.learn.gs.business.api.value.UserItem;
import com.mp.learn.gs.business.spi.value.UserState;

public class UserItemAssembler {

  public UserItem apply(UserState state) {
    return UserItem.create(state.id(), state.name());
  }
}
