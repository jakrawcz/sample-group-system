package com.mp.learn.gs.business.api.value;

import java.time.Instant;

import com.google.auto.value.AutoValue;

/**
 * Listing-suitable properties of a User that joined a specific Group.
 */
@AutoValue
public abstract class MemberItem {

  /**
   * Properties of the User.
   */
  public abstract UserItem user();

  /**
   * An {@link Instant} at which the User has joined a Group.
   */
  public abstract Instant joinInstant();

  /**
   * Creates an instance.
   */
  public static MemberItem create(UserItem user, Instant joinInstant) {
    return new AutoValue_MemberItem(user, joinInstant);
  } 
}
