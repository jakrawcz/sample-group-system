package com.mp.learn.gs.business.spi.value;

import java.time.Instant;

import com.google.auto.value.AutoValue;
import com.mp.learn.gs.business.api.value.UserId;

/**
 * A state of a User.
 */
@AutoValue
public abstract class UserState {

  /**
   * An id.
   */
  public abstract UserId id();
  
  /**
   * A name.
   */
  public abstract String name();
  
  /**
   * An {@link Instant} at which the User has signed up.
   */
  public abstract Instant signUpInstant();

  /**
   * Creates an instance.
   */
  public static UserState create(UserId id, String name, Instant signUpInstant) {
    return new AutoValue_UserState(id, name, signUpInstant);
  }
}
