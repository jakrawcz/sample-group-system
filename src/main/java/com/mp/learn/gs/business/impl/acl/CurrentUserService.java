package com.mp.learn.gs.business.impl.acl;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.CurrentUserProvider;
import com.mp.learn.gs.business.spi.service.UserDao;

public class CurrentUserService {

  private final CurrentUserProvider currentUserProvider;
  private final UserDao userDao;

  @Inject
  public CurrentUserService(CurrentUserProvider currentUserProvider, UserDao userDao) {
    this.currentUserProvider = currentUserProvider;
    this.userDao = userDao;
  }
  
  public UserId getCurrentUserId() {
    UserId currentUserId = getDeclaredCurrentUserId();
    checkUserExists(currentUserId);
    return currentUserId;
  }

  public UserId getDeclaredCurrentUserId() {
    return currentUserProvider.getCurrentUserId();
  }
  
  private void checkUserExists(UserId userId) {
    userDao.load(userId);
  }
}
