package com.mp.learn.gs.business.impl.acl;

import com.mp.learn.gs.business.api.ex.UserNotAuthorizedException;
import com.mp.learn.gs.business.api.value.UserId;

public class UserAuthorizer {

  private final UserId userId;

  public UserAuthorizer(UserId userId) {
    this.userId = userId;
  }

  public void checkCanDeleteUser(UserId anotherUserId) {
    checkThat(userId.equals(anotherUserId), "delete user %s", anotherUserId);
  }

  private void checkThat(boolean condition, String messageTemplate, Object... templateArguments) {
    if (!condition) {
      throw new UserNotAuthorizedException(messageTemplate, templateArguments);
    }
  }
}
