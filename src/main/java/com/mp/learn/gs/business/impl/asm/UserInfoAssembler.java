package com.mp.learn.gs.business.impl.asm;

import com.mp.learn.gs.business.api.value.UserInfo;
import com.mp.learn.gs.business.spi.value.UserState;

public class UserInfoAssembler {

  public UserInfo apply(UserState state) {
    return UserInfo.create(state.name(), state.signUpInstant());
  }
}
