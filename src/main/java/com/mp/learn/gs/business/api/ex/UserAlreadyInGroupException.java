package com.mp.learn.gs.business.api.ex;

/**
 * A {@link GroupBusinessLogicContingencyException contingency} thrown when a user tries to join a
 * group which they already are a member of. 
 */
public class UserAlreadyInGroupException extends GroupBusinessLogicContingencyException {

  private static final long serialVersionUID = -3171360173621685077L;
  
  public UserAlreadyInGroupException() {
  }
}
