package com.mp.learn.gs.business.api.service;

import java.util.List;

import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.GroupItem;

/**
 * A service governing Groups.
 */
public interface GroupService {

	/**
	 * Creates a Group of the given name and size limit.
	 */
	GroupId create(String name, int limit);
	
	/**
	 * Lists all the existing Groups.
	 */
	List<GroupItem> list();
	
	/**
	 * Returns a manager of a Group of the given id.
	 */
	GroupManager manage(GroupId groupId);
}
