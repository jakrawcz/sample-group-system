package com.mp.learn.gs.business.spi.service;

import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.value.UserState;

/**
 * A DAO for User states.
 */
public interface UserDao {

  /**
   * Returns a state of the given id.
   */
  UserState load(UserId userId);

  /**
   * Returns all the states of the given ids.
   */
  Iterable<UserState> loadBatch(Iterable<UserId> userIds);

  /**
   * Saves the given state.
   */
  void save(UserState state);

  /**
   * Deletes a state of the given id.
   */
  void delete(UserId userId);
}
