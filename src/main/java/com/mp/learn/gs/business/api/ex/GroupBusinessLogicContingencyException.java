package com.mp.learn.gs.business.api.ex;

/**
 * A contingency that may happen during normal GroupSystem BusinessLogic operation.
 */
public abstract class GroupBusinessLogicContingencyException extends RuntimeException {

  private static final long serialVersionUID = 5724852106652985095L;

  public GroupBusinessLogicContingencyException() {
  }

  public GroupBusinessLogicContingencyException(String messageTemplate,
      Object... templateArguments) {
    super(String.format(messageTemplate, templateArguments));
  }
}
