package com.mp.learn.gs.business.api.value;

import com.google.auto.value.AutoValue;

/**
 * Listing-suitable properties of a specific Group.
 */
@AutoValue
public abstract class GroupItem {

  /**
   * An id.
   */
  public abstract GroupId id();
  
  /**
   * A name.
   */
  public abstract String name();
  
  /**
   * A number of the Group's members.
   */
  public abstract int size();
  
  /**
   * Creates an instance.
   */
  public static GroupItem create(GroupId id, String name, int size) {
    return new AutoValue_GroupItem(id, name, size);
  }
}
