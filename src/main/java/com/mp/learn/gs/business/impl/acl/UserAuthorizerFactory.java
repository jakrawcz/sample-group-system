package com.mp.learn.gs.business.impl.acl;

import javax.inject.Inject;

public class UserAuthorizerFactory {

  private final CurrentUserService currentUserService;

  @Inject
  public UserAuthorizerFactory(CurrentUserService currentUserService) {
    this.currentUserService = currentUserService;
  }
  
  public UserAuthorizer createForCurrentUser() {
    return new UserAuthorizer(currentUserService.getCurrentUserId());
  }
}
