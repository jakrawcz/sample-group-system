package com.mp.learn.gs.business.impl;

import java.time.Clock;

import javax.inject.Inject;

import com.mp.learn.gs.business.api.service.UserManager;
import com.mp.learn.gs.business.api.service.UserService;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.spi.service.UserDao;
import com.mp.learn.gs.business.spi.value.UserState;

public class UserServiceImpl implements UserService {

  private final UserManagerFactory userManagerFactory;
  private final UserDao userDao;
  private final Clock clock;

  @Inject
  public UserServiceImpl(
      Clock clock,
      UserManagerFactory userManagerFactory, 
      UserDao userDao) {
    this.clock = clock;
    this.userManagerFactory = userManagerFactory;
    this.userDao = userDao;
  }
  
  @Override
  public UserManager manage(UserId userId) {
    return userManagerFactory.create(userId);
  }

  @Override
  public UserId create(String name) {
    UserId newUserId = UserId.createNew();
    userDao.save(UserState.create(
        newUserId,
        name,
        clock.instant()));
    return newUserId;
  }
}
