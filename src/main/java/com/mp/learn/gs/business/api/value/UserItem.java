package com.mp.learn.gs.business.api.value;

import com.google.auto.value.AutoValue;

/**
 * Listing-suitable properties of a specific User.
 */
@AutoValue
public abstract class UserItem {

  /**
   * An id.
   */
  public abstract UserId id();
  
  /**
   * A name.
   */
  public abstract String name();

  /**
   * Creates an instance.
   */
  public static UserItem create(UserId id, String name) {
    return new AutoValue_UserItem(id, name);
  }
}
