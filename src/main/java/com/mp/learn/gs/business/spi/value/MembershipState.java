package com.mp.learn.gs.business.spi.value;

import java.time.Instant;

import com.google.auto.value.AutoValue;
import com.mp.learn.gs.business.api.value.UserId;

/**
 * An indication that a User has joined a specific Group.
 */
@AutoValue
public abstract class MembershipState {

  /**
   * An id of the User.
   */
  public abstract UserId userId();

  /**
   * An {@link Instant} at which the User has joined a Group.
   */
  public abstract Instant joinInstant();
  
  /**
   * Creates an instance.
   */
  public static MembershipState create(UserId userId, Instant joinInstant) {
    return new AutoValue_MembershipState(userId, joinInstant);
  }
}
