package com.mp.learn.gs.business.spi.value;

import java.util.Optional;

import javax.annotation.Nullable;

import com.mp.learn.gs.business.api.value.UserId;

import autovalue.shaded.com.google.common.common.base.Preconditions;

/**
 * Criteria that may be imposed on a Group.
 */
public class GroupCriteria {

  @Nullable
  private UserId requiredMember;

  private GroupCriteria(@Nullable UserId requiredMember) {
    this.requiredMember = requiredMember;
  }

  private GroupCriteria(GroupCriteria prototype) {
    this(prototype.requiredMember);
  }

  private GroupCriteria() {
  }

  /**
   * An optional id of a User that must be a member of a Group.
   */
  public Optional<UserId> requiredMemberId() {
    return Optional.ofNullable(requiredMember);
  }
  
  /**
   * Creates always-met criteria.
   */
  public static GroupCriteria any() {
    return new GroupCriteria();
  }

  /**
   * Returns a new criteria with an additional requirement that a Group must contain a member of the
   * given id.
   */
  public GroupCriteria thatContainsMember(UserId userId) {
    Preconditions.checkState(requiredMember == null, "required member already set");
    GroupCriteria copy = new GroupCriteria(this);
    copy.requiredMember = userId;
    return copy;
  }
}
