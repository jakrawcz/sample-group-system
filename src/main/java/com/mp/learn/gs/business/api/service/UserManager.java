package com.mp.learn.gs.business.api.service;

import com.mp.learn.gs.business.api.value.UserInfo;

/**
 * A scoped manager of a single User.
 */
public interface UserManager {

  /**
   * Returns the User's details.
   */
  UserInfo show();
  
  /**
   * Deletes the User.
   */
  void delete();
}
