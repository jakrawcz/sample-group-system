package com.mp.learn.gs.business.impl;

import java.time.Clock;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.google.common.collect.ImmutableList;
import com.mp.learn.gs.business.api.service.GroupManager;
import com.mp.learn.gs.business.api.service.GroupService;
import com.mp.learn.gs.business.api.value.GroupId;
import com.mp.learn.gs.business.api.value.GroupItem;
import com.mp.learn.gs.business.api.value.UserId;
import com.mp.learn.gs.business.impl.acl.CurrentUserService;
import com.mp.learn.gs.business.impl.asm.GroupItemAssembler;
import com.mp.learn.gs.business.spi.service.GroupDao;
import com.mp.learn.gs.business.spi.service.Transactor;
import com.mp.learn.gs.business.spi.value.GroupCriteria;
import com.mp.learn.gs.business.spi.value.GroupState;
import com.mp.learn.gs.business.spi.value.MembershipState;

import autovalue.shaded.com.google.common.common.collect.Iterables;

public class GroupServiceImpl implements GroupService {

  private final CurrentUserService currentUserService;
  private final GroupManagerFactory groupManagerFactory;
  private final GroupDao groupDao;
  private final Clock clock;
  private final GroupItemAssembler groupItemAssembler;
  private final Transactor transactor;

  @Inject
  public GroupServiceImpl(
      Clock clock,
      CurrentUserService currentUserService,
      GroupManagerFactory groupManagerFactory, 
      GroupDao groupDao,
      Transactor transactor,
      GroupItemAssembler groupItemAssembler) {
    this.clock = clock;
    this.currentUserService = currentUserService;
    this.groupManagerFactory = groupManagerFactory;
    this.groupDao = groupDao;
    this.transactor = transactor;
    this.groupItemAssembler = groupItemAssembler;
  }
  
  @Override
  public GroupId create(String name, int limit) {
    GroupId newGroupId = GroupId.createNew();
    Instant now = clock.instant();
    transactor.transact(() -> {
      UserId creator = currentUserService.getCurrentUserId();
      List<MembershipState> memberships = Arrays.asList(MembershipState.create(creator, now));
      groupDao.save(GroupState.create(newGroupId, now, name, limit, memberships));
    });
    return newGroupId;
  }

  @Override
  public List<GroupItem> list() {
    return ImmutableList.copyOf(Iterables.transform(
        groupDao.query(GroupCriteria.any()), groupItemAssembler::apply));
  }

  @Override
  public GroupManager manage(GroupId groupId) {
    return groupManagerFactory.create(groupId);
  }
}
