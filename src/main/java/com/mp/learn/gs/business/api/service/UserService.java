package com.mp.learn.gs.business.api.service;

import com.mp.learn.gs.business.api.value.UserId;

/**
 * A service governing Users.
 */
public interface UserService {

  /**
   * Creates a User of the given name.
   */
  UserId create(String name);
  
	/**
	 * Returns a manager of a User of the given id.
	 */
	UserManager manage(UserId userId);
}
