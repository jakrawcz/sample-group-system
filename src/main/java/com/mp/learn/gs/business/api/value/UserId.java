package com.mp.learn.gs.business.api.value;

import java.util.UUID;

/**
 * An id of a User.
 */
public class UserId extends ExternalId {

  private UserId(UUID uuid) {
    super(uuid);
  }

  /**
   * Creates an instance from {@link UUID}.
   */
  public static UserId fromUuid(UUID uuid) {
    return new UserId(uuid);
  }

  /**
   * Creates a new, unique instance.
   */
  public static UserId createNew() {
    return fromUuid(UUID.randomUUID());
  }
}
